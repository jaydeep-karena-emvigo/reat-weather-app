import {useEffect, useState} from 'react'
import './App.css'
import Day from './components/day'
import {getWeatherData} from './services/getWeatherData'

function App() {
  const [loading, setLoading] = useState(true)
  const [error, setError] = useState('')
  const [weatherData, setWeatherData] = useState({})

  useEffect(() => {
    async function fetchData() {
      const {data, success, error} = await getWeatherData()
      if (success) {
        setWeatherData(data)
      } else {
        setError(error)
      }
      setLoading(false)
    }
    fetchData()
  }, [])

  if (loading) return <div>Loading...</div>

  const Weather = () => {
    const {name, country} = weatherData.location
    const {temp_c} = weatherData.current
    const hour = weatherData.forecast.forecastday[0].hour
    return (
      <div className='weather-container'>
        <h2>{weatherData?.current?.condition?.text || ''}</h2>
        <br />
        <div className='country-info'>{country} <br /> ({name}) {temp_c}°C</div>
        <br />
        <div className='days'>
          {hour.map(day => {
            let {temp_c, condition, time} = day
            return (
              <Day time={time} iconSrc={condition.icon} altTtext={condition.text} temp={temp_c} />
            )
          })}
        </div>
      </div>
    )
  }

  return (
    <div className='App'>
      <h1>Weather App</h1>
      {error ? <div className='error'>{error}</div> : null}
      {weatherData.location ? <Weather /> : null}
    </div>
  )
}

export default App
