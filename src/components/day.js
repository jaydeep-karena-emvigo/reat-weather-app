import React from 'react'
import {GetFormattedTime} from '../utils'

const Day = ({time, iconSrc, altTtext, temp}) => {
  let formattedTime = GetFormattedTime(time)

  return (
    <div key={time} className='day'>
      <span className='time'>{formattedTime}</span> <br />
      <img src={iconSrc} alt={altTtext} /> <br />
      <span className='temp'>{parseInt(temp)}°</span> <br />
    </div>
  )
}

export default Day
