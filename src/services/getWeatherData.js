import {APIS} from '../constants'

export async function getWeatherData() {
  try {
    const response = await fetch(APIS.weather)
    const data = await response.json()
    return {data, success: true, error: ''}
  } catch (error) {
    return {
      data: {},
      success: false,
      error: error?.message || 'Something went wrong while fetching weather data',
    }
  }
}
