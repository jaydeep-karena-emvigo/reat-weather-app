export function GetFormattedTime(time) {
  time = Number(time.slice(-5, -3))
  if (time > 12) {
    time = `${time - 12} PM`
  } else {
    time = `${time} AM`
  }
  return time
}
